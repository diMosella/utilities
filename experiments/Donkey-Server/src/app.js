const http = require('http');
const url = require('url');
const querystring = require('querystring');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((request, response) => {
  response.statusCode = 200;
  response.writeHead(200, { 'Content-Type': 'text/html' });
  response.write('<html><head><title>Donkey Test Title</title></head><body>Donkey Test Body</body>');
  response.end();

  const pathName = url.parse(request.url).pathname;
  const query = url.parse(request.url).query;
  const queryString = querystring.parse(query);
  console.log('pathName', pathName);
  console.log('query', query);
  console.log('queryString', queryString);
});

server.listen(port, hostname, () => {
  console.log(`Donkey Server running at http://${hostname}:${port}/`);
});
