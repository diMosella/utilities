import { html, css, LitElement, TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { repeat } from 'lit/directives/repeat.js';
import { classMap } from 'lit/directives/class-map.js';
import { ACCEPTABLE_MIME_TYPES, enumIterable, enumLookup, MIMEType } from './types/enum-types';
import { AugmentedFile, Dimensions, Hierarchy, Meta } from './types/common';
import { set, createStore, get } from 'idb-keyval';
import { getDocument, GlobalWorkerOptions, OPS } from 'pdfjs-dist';
import { createWorker } from 'tesseract.js';
import OcrWorker from './workers/ocrWorker?worker';
import { getDimensionsFromFile, getMimeTypeFromFileHeader } from './utils/fileExtractors';
import pdfWorkerUrl from './links/pdf.worker.js?url';
import tesseractWorkerUrl from './links/tesseract-worker.js?url';
import tesseractCoreUrl from './links/tesseract-core.js?url';

GlobalWorkerOptions.workerSrc = pdfWorkerUrl;

const validObjectTypes = [
  OPS.paintImageXObject, // 85
  OPS.paintImageXObjectRepeat, // 88
  OPS.paintJpegXObject //82
];

const fileStore = createStore('file-db', 'files');

/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
@customElement('dimosella-ocr')
export class OCR extends LitElement {
  static styles = css`
    :host {
      display: block;
      border: 1pt solid rgb(51, 143, 204);
      border-radius: .3em;
      padding: 16px;
      max-width: 800px;
      font-family: "Josefin Sans", sans-serif;
      font-weight: 300;
    }

    input[type=file] {
      display: none;
    }

    label.file {
      display: block;
      background: rgb(238, 246, 252);
      border: 1pt dashed rgb(51, 143, 204);
      border-radius: .3em;
      text-align: center;
      padding: 1em;
    }

    label.file:hover {
      cursor: pointer;
    }

    ul {
      padding: 0 1em;
    }

    li {
      font-weight: initial;
      line-height: 2.5em;
    }

    li.folder {
      font-weight: 600;
    }

    span {
      background: lightgoldenrodyellow;
      border-radius: .5em;
      font-size: .9em;
      font-style: italic;
      padding: .5em;
    }

    div {
      display: inline-block;
      text-decoration: inherit;
    }

    div.result {
      display: block;
      width: 50%;
      overflow-x: scroll;
    }

    canvas {
      transform: scale(0.3);
      transform-origin: top left;
    }

    li:not(.acceptable), span:not(.acceptable) {
      text-decoration: line-through;
    }
  `;

  /**
   * Select entire folder in stead of individual files
   */
  @property({ type: Boolean })
  isFolderSelector = false;

  @property({ type: Boolean })
  isEmptyUpload = false;

  @property({ attribute: false })
  selected = [] as Array<AugmentedFile>;

  constructor() {
    super();

    this._ocrWorker = new OcrWorker();
    this._ocrWorker.onmessage = (messageEvent) => {
      const data = messageEvent.data;
      console.log('from worker:', JSON.stringify(data));
    }

    this._tesseractWorker = createWorker({
      workerPath: tesseractWorkerUrl,
      langPath: './',
      corePath: tesseractCoreUrl,
      logger: (message) => console.log(message)
    });
  }

  render = () => {
    return html`
      <h1>Herken tekst van afbeeldingen of PDF-bestanden</h1>
      <label>
        Selecteer per map
        <input type='checkbox' .checked=${this.isFolderSelector} @change=${this._onFolderSelectorToggleChange}/>
      </label>
      <label class='file' @dragover=${this._onDrag} @drop=${this._onDrop}>
        <input @change=${this._onChange} type='file' multiple
            .webkitdirectory=${this.isFolderSelector} .directory=${this.isFolderSelector}
            accept=${enumIterable(MIMEType).map(item => item.name).join(',')} />
        Sleep ${this.isFolderSelector === true ? 'een map' : 'afbeeldingen of PDF-bestanden'} hierheen of klik voor een keuzevenster
      </label>
      ${this.selected.length === 0
        ? html`
          <h3>Nog geen bestanden geselecteerd</h3>
          ${!this.isFolderSelector && this.isEmptyUpload ? 'Er werd iets geselecteerd, maar dat waren geen bestanden' : ''}
        `
        : html`<h2>Geselecteerd</h2>
          ${this._renderHierarchy(this._inflateHierarchy(this.selected, ['type'], ['isFile', 'isAcceptable', 'type', 'relativePath']))}
        `
      }
      <div class='result'>
        <canvas></canvas>
      </div>
  </ol>

  </ul>
      <slot></slot>
    `;
  }

  private _ocrWorker: Worker;
  private _tesseractWorker: Tesseract.Worker;

  private _onFolderSelectorToggleChange = (toggleEvent: Event) => {
    toggleEvent.preventDefault();
    this.isFolderSelector = (toggleEvent.target as HTMLInputElement).checked;
  };

  private _onDrag = (dragEvent: DragEvent) => {
    dragEvent.preventDefault();
  };

  private _onChange = (inputEvent: Event) => {
    console.log('File(s) selected');
    inputEvent.preventDefault();

    if (inputEvent.target === null) {
      return;
    }

    this._handleFileInput(Array.from((inputEvent.target as HTMLInputElement).files || []));
  }

  private _onDrop = async (inputEvent: DragEvent) => {
    console.log('File(s) dropped');

    // Prevent default behavior (Prevent file from being opened)
    inputEvent.preventDefault();

    if (inputEvent.dataTransfer === null || typeof inputEvent.dataTransfer === 'undefined') {
      return;
    }

    if (inputEvent.dataTransfer.items) {
      // Use DataTransferItemList interface to access the file(s)
      const transferItems = Array.from(inputEvent.dataTransfer.items);
      if (this.isFolderSelector === true) {
        // Traverse folders
        const fileSystemItems = transferItems
          .map(item => item.webkitGetAsEntry())
          .filter(item => item !== null) as Array<FileSystemEntry>;
        await Promise.all(fileSystemItems.map((item, _index, parent) => this._scanDescendants(item, parent)));

        const fileItems: Array<File> = await Promise.all(fileSystemItems
          .filter(item => item.isFile)
          .map(item => new Promise<File>((resolve, reject) => {
            (item as FileSystemFileEntry).file(result => resolve(result), error => reject(error.message));
          })));
        console.log(fileItems);
        this._handleFileInput(fileItems);
      } else {
        // Only explicitly selected files
        this._handleFileInput(transferItems
          .map(item => item.kind === 'file' && item.webkitGetAsEntry()?.isFile ? item.getAsFile() : null)
          .filter(item => item !== null) as Array<File>);
      }
    } else {
      // Use DataTransfer interface to access the file(s)
      this._handleFileInput(Array.from(inputEvent.dataTransfer.files));
    }
  };

  private _handleFileInput = async (fileItems: Array<File>) => {
    console.info(`${fileItems.length} bestanden`);
    if (fileItems.length === 0) {
      this.isEmptyUpload = true;
      return;
    }
    this.isEmptyUpload = false;
    const augmentedFileItems = await Promise.all(fileItems.map(async item => {
      const type = await getMimeTypeFromFileHeader(item);
      return {
        blob: item,
        meta: {
          type,
          isAcceptable: (item.type === '' || enumLookup(MIMEType, item.type, MIMEType.UNKNOWN) === type) &&
            ACCEPTABLE_MIME_TYPES.includes(type),
          isFile: true,
          relativePath: item.webkitRelativePath || item.name,
          dimensions: MIMEType[type].toLowerCase().startsWith('image') && type !== MIMEType['IMAGE/X-SONY-ARW']
            ? await getDimensionsFromFile(item)
            : { width: 0, height: 0 } as Dimensions
        } as Meta
      } as AugmentedFile;
    }));
    console.log(augmentedFileItems[0].meta);
    this.selected = [...this.selected, ...augmentedFileItems];
    await Promise.all(augmentedFileItems
      .filter(item => item.meta.type !== MIMEType.UNKNOWN && item.meta.isAcceptable === true)
      .map(item => [
        set(`${item.meta.relativePath}/raw`, item.blob, fileStore),
        set(`${item.meta.relativePath}/meta`, item.meta, fileStore)
      ]).flat());
    this._ocrWorker.postMessage(augmentedFileItems
      .filter(item => item.meta.type !== MIMEType.UNKNOWN && item.meta.isAcceptable === true)
      .map(item => `${item.meta.relativePath}/meta`));

    const firstFile = augmentedFileItems[0];
    if (firstFile.meta.type === MIMEType['APPLICATION/PDF']) {
      console.log('fF', firstFile);
      const fileData = new Uint8Array(await firstFile.blob.arrayBuffer());
      const pdfDocument = await getDocument(fileData).promise;
      console.log(pdfDocument.numPages);

      try {
        const pdfPage = await pdfDocument.getPage(1);
        const operatorList = await pdfPage.getOperatorList();

        operatorList.fnArray.forEach((element, idx) => {
          if (validObjectTypes.includes(element)) {
            const imageName = operatorList.argsArray[idx][0];
            console.log('page', 0, 'imageName', imageName);

            pdfPage.objs.get(imageName, async (image: { data: Uint8ClampedArray; width: number; height: number; }) => {
              // Uint8ClampedArray
              const imageUint8Array = image.data;
              const imageWidth = image.width;
              const imageHeight = image.height;

              console.log('image', image);

              // imageUnit8Array contains only RGB need add alphaChanel
              const imageUint8ArrayWithAlphaChanel = this._addAlphaChannelToUnit8ClampedArray(imageUint8Array, imageWidth, imageHeight);

              const asFile = new File([imageUint8ArrayWithAlphaChanel.buffer], imageName);

              const imageData = new ImageData(imageUint8ArrayWithAlphaChanel, imageWidth, imageHeight);

              const canvas = document.querySelector('dimosella-ocr')?.shadowRoot?.querySelector('canvas');
              console.log(canvas);
              if (canvas) {
                canvas.width = imageWidth;
                canvas.height = imageHeight;
                const ctx = canvas.getContext('2d');
                if (ctx) {
                  ctx.putImageData(imageData, 0, 0);
                }

                await this._tesseractWorker.load();
                await this._tesseractWorker.loadLanguage('nld');
                await this._tesseractWorker.initialize('nld');
                const { data: { text } } = await this._tesseractWorker.recognize(canvas);
                console.log(text);
                await this._tesseractWorker.terminate();
              }
            });
          }
        });
      } catch (error) {
        console.log(error);
      }
    }
    if (firstFile.meta.type === MIMEType['IMAGE/JPEG'] && firstFile.meta.dimensions) {
      console.log('fF', firstFile);
      const fileData = new Uint8ClampedArray(await firstFile.blob.arrayBuffer());

      await this._tesseractWorker.load();
      await this._tesseractWorker.loadLanguage('nld');
      await this._tesseractWorker.initialize('nld');
      const { data: { text } } = await this._tesseractWorker.recognize(firstFile.blob);
      console.log(text);
      await this._tesseractWorker.terminate();
    }
  };

  private _scanDescendants = async (entry: FileSystemEntry, parent: Array<FileSystemEntry>) => {
    if (entry.isDirectory) {
      const reader = (entry as FileSystemDirectoryEntry).createReader();
      // readEntries only returns first number (ca. 100) of entries
      let isDone = false;
      while (!isDone) {
        const nestedEntries = await new Promise<FileSystemEntry[]>((resolve, reject) => {
          reader.readEntries(result => resolve(result), error => reject(error.message));
        });
        if (nestedEntries.length === 0) {
          isDone = true;
        } else {
          isDone = false;
          parent.push(...nestedEntries);
          await Promise.all(nestedEntries.map(entry => this._scanDescendants(entry, parent)));
        }
      }
    }
  };

  private _inflateHierarchy = (fileList: Array<AugmentedFile>,
    blobProperties: Array<keyof File> = ['name'],
    metaProperties: Array<keyof Meta> = ['type']) => {
    let hierarchy = {} as Hierarchy;
    fileList.map(async file => {
      const pathParts = file.meta.relativePath.split('/');

      const fileInfo = {
        blob: blobProperties.reduce((accumulator, propertyName) =>
          Object.assign({}, accumulator, { [propertyName]: file.blob[propertyName] }),
          {} as Record<string, Partial<File>>),
        meta: metaProperties.reduce((accumulator, propertyName) =>
          Object.assign({}, accumulator, { [propertyName]: file.meta[propertyName] }),
          {} as Record<string, Partial<Meta>>)
      };

      // determine where in the hierarchy the info should be merged
      const hierarchyPointers = this._getNodePointers(hierarchy);
      const commonPointer = this._greatestCommonPointer(pathParts, hierarchyPointers);
      const startIndex = commonPointer.length;

      const fileHierarchy = pathParts.reduceRight((accumulator, part, index) =>
        index > startIndex
          ? Object.assign({}, { [part]: accumulator })
          : accumulator,
        fileInfo as Hierarchy);

      // merge info in existing hierarchy
      const hierarchySiblingInfo = commonPointer.reduce((accumulator, pointerPart) => accumulator[pointerPart] as Hierarchy, hierarchy);
      hierarchySiblingInfo[pathParts[startIndex]] = fileHierarchy;
    });
    return hierarchy;
  };

  private _getNodePointers = (entity: Object) => {
    const pointers = [];
    const entries = Object.entries(entity).filter(entry => typeof entry[1] === 'object');
    pointers.push(...entries.map(entry => [entry[0]]));
    entries.forEach(entry => {
      pointers.push(...this._getNodePointers(entry[1]).map(descendantPointer => [entry[0], ...descendantPointer]));
    });
    return pointers;
  };

  private _greatestCommonPointer = (pointer: Array<string>, pointerCollection: Array<Array<string>>) => {
    let filtered = pointerCollection;
    let index = -1;
    while (filtered.length > 0 || index === -1) {
      index++;
      filtered = filtered.filter(item => item[index] === pointer[index]);
    }
    return pointer.slice(0, index);
  };

  private _renderHierarchy = (hierarchy: Hierarchy): TemplateResult => {
    const meta = hierarchy.meta as Partial<Meta>;
    const blob = hierarchy.blob as Partial<File>;

    return meta && blob && meta.isFile === true
      ? html`
        <span class=${classMap({ acceptable: meta.isAcceptable === true })}
          title=${!meta.isAcceptable ? 'Dit bestandstype zal niet worden verwerkt. Het verwerken van dit type is niet geconfigureerd.' : ''}>
          ${blob.type || 'unknown'} | ${meta.type ? MIMEType[meta.type].toLowerCase() : 'unknown'}
        </span>
      `
      : html`
        <ul>
          ${repeat(Object.entries(hierarchy), entry => {
        const value = entry[1] as Hierarchy;
        const meta = value.meta as Partial<Meta>;
        return html`
              <li class=${classMap({ folder: !meta || meta.isFile !== true, acceptable: !meta || meta.isAcceptable === true })}>
                <div title=${meta && !meta.isAcceptable ? 'Dit bestandstype zal niet worden verwerkt. Het verwerken van dit type is niet geconfigureerd.' : ''}>
                  ${entry[0]}
                </div>
                ${this._renderHierarchy(entry[1] as Hierarchy)}
              </li>
            `
      }
      )}
        </ul>
        `
  };

  private _addAlphaChannelToUnit8ClampedArray(unit8Array: Uint8ClampedArray, imageWidth: number, imageHeight: number) {
    const newImageData = new Uint8ClampedArray(imageWidth * imageHeight * 4);

    for (let j = 0, k = 0, jj = imageWidth * imageHeight * 4; j < jj;) {
      newImageData[j++] = unit8Array[k++];
      newImageData[j++] = unit8Array[k++];
      newImageData[j++] = unit8Array[k++];
      newImageData[j++] = 255;
    }

    return newImageData;
  };
}

declare global {
  interface HTMLElementTagNameMap {
    'dimosella-ocr': OCR
  }
}
