import { Dimensions } from "../types";
import { mapToMimeType } from "../types/enum-types";

const _fileHeaderReader = async (file: File) => {
  const raw = await file.slice(0, 4).arrayBuffer();
  const bytes = new Uint8Array(raw);
  let fileHeader = '';
  for (let index = 0; index < bytes.length; index++) {
    fileHeader += bytes[index].toString(16);
  }
  return fileHeader;
};

const _dimensionReader = (dataURL: string) => new Promise<Dimensions>((resolve, _reject) => {
  const image = new Image();
  image.addEventListener('load', () => {
    resolve({
      width: image.width,
      height: image.height
    } as Dimensions);
  });
  image.src = dataURL;
});

const _fileReader = (file : File) => new Promise<string>((resolve, _reject) => {
  const reader = new FileReader();
  reader.addEventListener('loadend', () => {
    resolve(reader.result === null ? '' : reader.result.toString());
  });
  reader.readAsDataURL(file);
});

const getMimeTypeFromFileHeader = async (file: File) =>
  mapToMimeType(await _fileHeaderReader(file), file.name.split('.').slice(-1)[0]);

const getDimensionsFromFile = async (file: File) =>
  await _dimensionReader(await _fileReader(file));

export {
  getMimeTypeFromFileHeader, getDimensionsFromFile
};

