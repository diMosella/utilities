
// TODO: in a future version use MIME-lib like *mime* (https://github.com/broofa/mime#readme)
// or *mrmime* (https://github.com/lukeed/mrmime)
// FIXME: for now, it is required to always start at a non-zero index
export enum MIMEType {
  'APPLICATION/PDF' = 10,
  'IMAGE/GIF',
  'IMAGE/BMP',
  'IMAGE/PNG',
  'IMAGE/TIFF',
  'IMAGE/JPEG',
  'IMAGE/X-SONY-ARW',
  'IMAGE/SVG+XML',
  'VIDEO/MP2T',
  'VIDEO/QUICKTIME',
  'APPLICATION/ZIP',
  'APPLICATION/MSWORD',
  'APPLICATION/VND.OPENXMLFORMATS-OFFICEDOCUMENT.WORDPROCESSINGML.DOCUMENT',
  'TEXT/MARKDOWN',
  'UNKNOWN'
}

export const mapToMimeType = (fileHeader: string, extension = '') => {
  switch (fileHeader) {
    case '89504e47':
      return MIMEType['IMAGE/PNG'];
    case '47494638':
      return MIMEType['IMAGE/GIF'];
    case '25504446':
      return MIMEType['APPLICATION/PDF'];
    case 'ffd8ffdb':
    case 'ffd8ffe0':
    case 'ffd8ffe1':
    case 'ffd8ffe2':
    case 'ffd8ffe3':
      return MIMEType['IMAGE/JPEG'];
    case '49492a0':
      return MIMEType['IMAGE/X-SONY-ARW'];
    case '0eace40':
    case '0eba8b0':
    case '0ebf560':
    case '0edbeb0':
      return MIMEType['VIDEO/MP2T'];
    case '00014':
      return MIMEType['VIDEO/QUICKTIME'];
    case '3c3f786d':
    case '3c737667':
      return MIMEType['IMAGE/SVG+XML'];
    case '504b0304':
    case '504b34':
      return MIMEType['APPLICATION/ZIP'];
    case '2320416c':
    case '23204d46':
      return MIMEType['TEXT/MARKDOWN'];
    default:
      console.info(`Filetype '${fileHeader}' is unknown for file extension '${extension}'`);
      return MIMEType.UNKNOWN;
  }
};

export const ACCEPTABLE_MIME_TYPES = [
  MIMEType['APPLICATION/PDF'],
  MIMEType['IMAGE/JPEG'], MIMEType['IMAGE/PNG'], MIMEType['IMAGE/X-SONY-ARW'],
  MIMEType['VIDEO/QUICKTIME'], MIMEType['VIDEO/MP2T']
];

// Warning: this does only work for non zero starting enums!
export const enumLookup = <E>(enumInstance: E, value: string, defaultValue: E[keyof E]): E[keyof E] =>
  enumInstance[<keyof typeof enumInstance>value.replace(/([^A-Z])([A-Z])/g, '$1_$2').toUpperCase()] || defaultValue;

export const enumIterable = <E, K extends keyof E>(enumInstance: E): Array<{ name: string, value: E[K] }> =>
  Object.keys(enumInstance).filter(enumKey => Number.isNaN(+enumKey))
    .map(enumKey =>
      ({ name: enumKey.toLowerCase().replace(/_([^_])/g, (_match, lowerCase) => lowerCase.toUpperCase()), value: enumInstance[enumKey as K] }));
