'use strict';

import { MIMEType } from "./enum-types";

interface Dimensions {
  width: number,
  height: number
};

interface Meta {
  // name: string,
  // size: number,
  // lastModified: number,
  isFile: boolean,
  isAcceptable: boolean,
  type: MIMEType,
  relativePath: string,
  dimensions?: Dimensions
}

interface AugmentedFile {
  blob: File,
  meta: Meta
}

interface Hierarchy extends Record<string, string | Partial<File> | Partial<Meta>> {}

export {
  AugmentedFile, Meta, Hierarchy, Dimensions
};
