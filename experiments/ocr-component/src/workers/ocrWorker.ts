import { get, set, createStore } from 'idb-keyval';
import MediaInfo from 'mediainfo.js';
import MediaInfoModule from './workers/MediaInfoModule.wasm?url';
import { Meta } from '../types';
import { MIMEType } from '../types/enum-types';



// FIXME: workaround for vitejs assuming in main thread
class HTMLElement {
  constructor() { };
  new = () => { };
  prototype = {};
};

class CustomElements {
  constructor() { };
  get = () => [];
}

// @ts-ignore:next-line
self.HTMLElement = HTMLElement;
// @ts-ignore:next-line
self.customElements = new CustomElements();

const exifr = await import('exifr');
// end FIXME: workaround

const readChunk = (file : File) => (chunkSize : number, offset : number) : Promise<Uint8Array> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = (event) => {
      if (event.target) {
        if (event.target.error) {
          reject(event.target.error)
        }
        if (event.target.result !== null && typeof event.target.result !== 'string') {
          resolve(new Uint8Array(event.target.result))
        }
      }
    }
    reader.readAsArrayBuffer(file.slice(offset, offset + chunkSize))
  });

const ocrWorker = self as DedicatedWorkerGlobalScope & typeof globalThis;

const fileStore = createStore('file-db', 'files');

ocrWorker.addEventListener('message', async (event) => {
  console.log('worker reads', JSON.stringify(event.data, null, 2));
  const processableData = event.data as Array<String>;
  if (processableData.length === 0) {
    return;
  }
  const firstFileMeta = await get<Meta>(processableData[0] as IDBValidKey, fileStore);
  const firstFile = await get<File>(`${processableData[0].split('/').slice(0, -1).join('/')}/raw` as IDBValidKey, fileStore);
  console.log('first file meta:', firstFileMeta);
  console.log('first file:', firstFile);
  if (firstFileMeta && MIMEType[firstFileMeta.type].toLowerCase().startsWith('image')) {
    const firstFileMetaExif = await exifr.parse(firstFile, true);
    await set(processableData[0] as IDBValidKey, Object.assign({}, firstFileMeta, { exif: firstFileMetaExif }), fileStore);
  }
  if (firstFile && firstFileMeta &&
      (MIMEType[firstFileMeta.type].toLowerCase().startsWith('video') || MIMEType[firstFileMeta.type].toLowerCase().startsWith('audio'))) {
    const mediaInfo = await MediaInfo({ format: 'JSON' });

    const result = await mediaInfo.analyzeData(() => firstFile ? firstFile.size : 0, readChunk(firstFile));
    console.info(result);
  }

  postMessage((processableData).map(file => file));
}, false);

export type { };
