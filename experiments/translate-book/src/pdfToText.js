const fs = require('fs');
const pdf = require('pdf-parse');

console.log(process.cwd());

let dataBuffer = fs.readFileSync('./data/book.pdf');
 
pdf(dataBuffer).then(function(data) {
 
    // number of pages
    console.log(data.numpages);
    // number of rendered pages
    console.log(data.numrender);
    // PDF info
    console.log(data.info);
    // PDF metadata
    console.log(data.metadata); 
    // PDF.js version
    // check https://mozilla.github.io/pdf.js/getting_started/
    console.log(data.version);
    // PDF text
    fs.writeFile('./data/book.txt', data.text, () => console.log('done writing'));
});