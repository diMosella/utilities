const translate = require('@vitalets/google-translate-api');
const fs = require('fs');

let dataBuffer = fs.readFileSync('./data/book.txt');

const chapters = dataBuffer.toString().split(/CHAPTER \d{1,2}/);

chapters.forEach((chapter, chIndex) => {
    if (chIndex !== 7) { return };
    const reg = new RegExp(`\\n${chIndex}.\\d`);
    const regMatch = new RegExp(reg.toString().replace(/^\//, '').replace(/\/$/, ''), 'g');
    const paragraphs = chapter.split(reg);
    console.log(chIndex, ':', paragraphs.length, reg.toString(), regMatch.toString());

    const numbers = [...chapter.matchAll(regMatch)].map(number => number[0]);
    console.log('numbers', ...numbers);
    const result = new Array(paragraphs.length).fill(false);
    const done = new Array(paragraphs.length).fill(false);

    const testFinal = () => {
        if (done.every(item => item === true)) {
            console.log('done', chIndex);
            fs.writeFile(`./data/chapter${chIndex}.txt`, result.join('\n\n'), () => console.log('done writing', chIndex));
        }
    }
    paragraphs.forEach((paragraph, index) => {
        translate(`${index > 0 ? `\n${numbers[index - 1]}` : `CHAPTER ${chIndex}`}${paragraph}`, {from: 'en', to: 'nl'}).then(res => {
            result[index] = res.text;
            done[index] = true;
            testFinal();
        }).catch(err => {
            console.error(err);
        })
    });
});
