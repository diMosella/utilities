const http   = require('http');
const querystring = require('querystring');
const fs      = require('fs');
const jsdom   = require('jsdom');
const immer = require('immer');

const { JSDOM } = jsdom;
const { produce } = immer;

const initialAlbum = {
  output: null,
  config: 'data.knmi.nl',
  framename: 'mainright',
  year: '2013', // < TYPE A YEAR AS STRING >
  month: '01' // < TYPE A MONTH NUMBER AS STRING >
}

loadAlbum(initialAlbum, 'main');

function nextMonth(album) {
  const month = (parseInt(album.month) % 12 + 1).toString().padStart(2, '0');
  const year = month === '01'
    ? (parseInt(album.year) + 1).toString().padStart(4, '0')
    : album.year;
  return produce(album, draftState => Object.assign(draftState, { month, year }));
}

function loadAlbum(albumIn, output) {
  const album = produce(albumIn, draftState => Object.assign(draftState, { output }));
  const albumQuery = querystring.stringify(album);

  let optionsAlbum = {
    hostname  : 'awstats.knmi.nl',
    port      : 80,
    path      : '/awstats/awstats.pl',
    method    : 'GET',
    headers: {
      'Accept': '*/*',
      'Accept-Language': 'en-US,en;q=0.8',
      'Cache-Control': 'no-cache',
      'Connection': 'keep-alive',
      'Content-Length': Buffer.byteLength(albumQuery),
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Host': 'awstats.knmi.nl',
      'Origin': 'http://awstats.knmi.nl',
      'Referer': 'http://awstats.knmi.nl',
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/58.0.3029.110 Chrome/58.0.3029.110 Safari/537.36',
      'X-Requested-With': 'XMLHttpRequest'
    }

  };

  let folder = `results/${album.year}${album.month}`;

  if(!fs.existsSync(folder)) {
    console.log('SCRAPE FOLDER doesn\'t yet exist and will be created');
    fs.mkdirSync(folder);
  }

  let albumData = '';
  let fileName = `${folder}/scrape-${output}.html`;
  if(fs.existsSync(fileName)) {
    console.error(`SCRAPE ${fileName} already exists`);
    return;
  }
  let fileAlbum = fs.createWriteStream(fileName);

  let requestAlbum = http.request(optionsAlbum, (response) => {
    console.log("SCRAPE RESPONSE:", response.statusCode, `${album.year}${album.month}-${output}`);
    // console.log("SCRAPE RESPONSE: headers = ", response.headers, `${output}`);

    response.on('data', (data) => {
      // console.log(`SCRAPE DATA RECEIVED for ${album.year}${album.month}-${output}`);
      fileAlbum.write(data);
      albumData += data;
    });

    response.on('end', () => {
      // console.log('SCRAPE DATA: No more data in response.');
      fileAlbum.close();
      if (output !== 'main') {
        return;
      }
      const { document } = new JSDOM(`${albumData}`).window;
      const PREFIX = 'awstats.pl?';
      [...document.querySelectorAll("a")]
        .map(el => el.getAttribute('href'))
        .filter(link => typeof link === 'string' && link.startsWith(PREFIX))
        .map(link => link.substring(PREFIX.length))
        .map(link => querystring.parse(link).output)
        .forEach(linkOutput => loadAlbum(album, linkOutput));
      if (parseInt(album.year) < 2019 || parseInt(album.month) < 11) {
        setTimeout(loadAlbum, 2500, nextMonth(album), output);
      }
      // console.log(`${output}`, nextMonth(album));
    });
  });

  requestAlbum.on('error', (error) => {
    console.log(`SCRAPE: Problem with request: ${error.message}`);
  });

  requestAlbum.write(albumQuery);
  requestAlbum.end();
}
