const fs      = require('fs');
const querystring = require('querystring');
const jsdom   = require('jsdom');
const immer = require('immer');

const { JSDOM } = jsdom;
const { produce } = immer;

const initialAlbum = {
  output: null,
  year: '2013', // < TYPE A YEAR AS STRING >
  month: '01' // < TYPE A MONTH NUMBER AS STRING >
}

loadAlbum(initialAlbum, 'main');

function nextMonth(album) {
  const month = (parseInt(album.month) % 12 + 1).toString().padStart(2, '0');
  const year = month === '01'
    ? (parseInt(album.year) + 1).toString().padStart(4, '0')
    : album.year;
  return produce(album, draftState => Object.assign(draftState, { month, year }));
}

function loadAlbum(albumIn, output) {
  const album = produce(albumIn, draftState => Object.assign(draftState, { output }));

  let folder = `results/${album.year}${album.month}`;

  if(!fs.existsSync(folder)) {
    console.error('CONVERT FOLDER doesn\'t yet exist!');
    return;
  }

  const fileName = `${folder}/scrape-${output}.html`;

  if(!fs.existsSync(fileName)) {
    console.error('CONVERT FILE doesn\'t exist!');
    return;
  }

  const albumData = fs.readFileSync(fileName);
  
  const { document } = new JSDOM(`${albumData}`).window;

  const tables = Array.from(document.querySelectorAll('table .aws_title'))
    .filter(elmt => elmt.querySelector('a') === null)
    .map(elmt => elmt.closest('table'));

  const names = [];
  if (tables.length === 1) {
    names.push(`${output}`);
  } else {
    const anchors = tables.map(elmt => elmt.previousElementSibling.previousElementSibling.getAttribute('name'))
      .map(name => name === 'menu' ? 'summary' : name);
    names.push(...anchors);
  }

  const data = tables
    .map(table => [...table.querySelectorAll('.aws_data > tbody > tr, .aws_data table:last-of-type tr')]
      .filter(row => row.childElementCount !== 1)
      .map(row => [...row.querySelectorAll('td, th')]
        .filter(col => col.querySelector('center table, table') === null)
        .map(col => col.textContent.trim())));

  data.forEach((table, index) => {
    const writefileName = `${folder}/convert-${names[index]}.csv`;
    if(!fs.existsSync(writefileName)) {
      const fileAlbum = fs.createWriteStream(writefileName);
      fileAlbum.write(table.map(row => row.join(',')).join('\n'));
      fileAlbum.close();
    }
  });

  if (output !== 'main') {
    return;
  }

  const PREFIX = 'awstats.pl?';
  [...document.querySelectorAll("a")]
    .map(el => el.getAttribute('href'))
    .filter(link => typeof link === 'string' && link.startsWith(PREFIX))
    .map(link => link.substring(PREFIX.length))
    .map(link => querystring.parse(link).output)
    .forEach(linkOutput => loadAlbum(album, linkOutput));

  if (parseInt(album.year) < 2019 || parseInt(album.month) < 11) {
    setTimeout(loadAlbum, 2500, nextMonth(album), output);
  }
  return;

  let requestAlbum = http.request(optionsAlbum, (response) => {
    console.log("SCRAPE RESPONSE:", response.statusCode, `${album.year}${album.month}-${output}`);
    // console.log("SCRAPE RESPONSE: headers = ", response.headers, `${output}`);

    response.on('data', (data) => {
      // console.log(`SCRAPE DATA RECEIVED for ${album.year}${album.month}-${output}`);
      fileAlbum.write(data);
      albumData += data;
    });

    response.on('end', () => {
      // console.log('SCRAPE DATA: No more data in response.');
      if (output !== 'main') {
        return;
      }
      const { document } = new JSDOM(`${albumData}`).window;
      const PREFIX = 'awstats.pl?';
      [...document.querySelectorAll("a")]
        .map(el => el.getAttribute('href'))
        .filter(link => typeof link === 'string' && link.startsWith(PREFIX))
        .map(link => link.substring(PREFIX.length))
        .map(link => querystring.parse(link).output)
        .forEach(linkOutput => loadAlbum(album, linkOutput));
      if (parseInt(album.year) < 2019 || parseInt(album.month) < 11) {
        setTimeout(loadAlbum, 2500, nextMonth(album), output);
      }
      // console.log(`${output}`, nextMonth(album));
    });
  });

  requestAlbum.on('error', (error) => {
    console.log(`SCRAPE: Problem with request: ${error.message}`);
  });

  requestAlbum.write(albumQuery);
  requestAlbum.end();
}
