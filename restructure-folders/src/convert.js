const fs = require('fs');
const path = require('path');
const escapeRegExp = require('lodash.escaperegexp');

const monthMap = {
  januari: '01',
  februari: '02',
  maart: '03',
  april: '04',
  mei: '05',
  juni: '06',
  juli: '07',
  augustus: '08',
  september: '09',
  oktober: '10',
  november: '11',
  december: '12'
};

const mapInverse = (map) => {
  const result = {};
  Object.entries(map).forEach(entry => { result[entry[1]] = entry[0]; });
  return result;
};

const convertMapToRegExpOptions = (map) => {
  return Object.keys(map).map(elmt => escapeRegExp(elmt)).join('|');
};

const getMapValue = (name, mapToUse, allCaps = false) => {
  if (name !== null && typeof name === 'string') {
    return mapToUse.hasOwnProperty(allCaps ? name.toUpperCase() : name) ? mapToUse[allCaps ? name.toUpperCase() : name] : null;
  }
  return null;
};

const nameRegEx = new RegExp('([^\\d]*)(\\d{1,2})(' + convertMapToRegExpOptions(monthMap) + ')(\\d{4})$', 'i');

const convertFolderName = (folderName) => {
  let result = null;
  const matchResult = folderName.replace(/\s/g, '').replace(/,/g, '_').match(nameRegEx);
  if (matchResult) {
    result = `${matchResult[4]}${getMapValue(matchResult[3], monthMap)}${matchResult[2].length < 2 ? `0` : ``}${matchResult[2]}${matchResult[1] ? `-${matchResult[1]}` : ``}`;
    result = result.replace(/_$/g, '');
  }
  return result;
};

const traverseDir = (dirPath) => {
  const isResources = false;
  fs.readdir(dirPath, (error, items) => {
    if (error) {
      console.error('Couldn\'t read directory', error);
      process.exit(-1);
    }
    console.log('Found %i items at path \'%s\'', items.length, dirPath);
    items.forEach((item) => {
      if (!!item.match(/^status/)) {
        return;
      }
      const itemString = isResources && item.indexOf('.') === -1
        ? `${item}/` // resolves symbolic links
        : item;
      const itemPath = path.join(dirPath, itemString);
      fs.lstat(itemPath, (error, stats) => {
        if (error) {
          console.error('Couldn\'t read stats', error);
          process.exit(-1);
        }
        if (stats.isDirectory()) {
          const newName = convertFolderName(itemString);
          if (newName) {
            fs.renameSync(itemPath, path.join(dirPath, newName));
          }
        }
      });
    });
  });
};

if (process.argv.length <= 2) {
    console.error('Usage: node %s path/to/directory', __filename);
    process.exit(-1);
}
 
const topLevelPath = process.argv[2];
if (!fs.existsSync(topLevelPath)){
  console.error('Provided string is not a filesystem item' , topLevelPath);
  process.exit(-1);
}

traverseDir(topLevelPath);
 