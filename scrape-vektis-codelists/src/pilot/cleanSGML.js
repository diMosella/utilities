elmt = document.getElementById("identificatie").parentNode;

Array.from(elmt.getElementsByTagName("br"))
	.forEach(entry => entry.parentNode.removeChild(entry));
Array.from(elmt.querySelectorAll('[aria-hidden]'))
	.forEach(entry => entry.removeAttribute('aria-hidden'));
Array.from(elmt.getElementsByTagName("a"))
	.forEach(entry => {
		const parent = entry.parentNode;
		const newLink = document.createElement('a');
		newLink.setAttribute('href', entry.getAttribute('href'));
		console.log('deb', entry.nodeValue || 'no', entry.innerText || 'no', entry.textContent);
		newLink.appendChild(document.createTextNode(entry.innerText.trim()));
		parent.removeChild(entry);
		parent.appendChild(newLink);
	});

result = `<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="codeList-20191210.xsl"?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
	</head>
	<body>
${elmt.innerHTML.replace(/[ \t]{2,}/gi, '').replace(/[\n]{2,}/gi, '\n').replace(/\&nbsp\;/gi, '')}
	</body>
</html>`;


codeList = document.createElement('code-list');
idElmt = document.getElementById('identificatie');
usageElmt = document.getElementById('gebruikt-in-standaarden');
contentElmt = document.getElementById('inhoud');
codeList.setAttribute('id', Array.from(idElmt.querySelector('p:nth-of-type(2)').childNodes).pop().nodeValue.trim());
meta = document.createElement('metadata');
meta.setAttribute('publication-date', Array.from(idElmt.querySelector('.col-sm-6:last-child p:nth-of-type(1)').childNodes).pop().nodeValue.trim().split('-').reverse().join('-'));
meta.setAttribute('maintainer', Array.from(idElmt.querySelector('.col-sm-6:last-child p:last-child').childNodes).pop().nodeValue.trim());
codeName = document.createElement('code-name');
nameValue = document.createTextNode(Array.from(idElmt.querySelector('p:first-child').childNodes).pop().nodeValue.trim());
codeName.appendChild(nameValue);
codeName.setAttribute('lang', 'nl');
meta.appendChild(codeName);
description = document.createElement('description');
descriptionValue = document.createTextNode(Array.from(idElmt.querySelector('p:last-child').childNodes).pop().nodeValue.trim());
description.appendChild(descriptionValue);
description.setAttribute('lang', 'nl');
meta.appendChild(description);
usageList = document.createElement('usages');
Array.from(usageElmt.querySelectorAll('a')).forEach((link) => {
	usage = document.createElement('usage');
	usage.setAttribute('url', link.getAttribute('href'));
	usage.setAttribute('id', link.querySelector('.specs__code').innerText.trim());
	usageName = document.createElement('usage-name');
	nameValue = document.createTextNode(link.querySelector('.specs__label').innerText.trim());
	usageName.appendChild(nameValue);
	usageName.setAttribute('lang', 'nl');
	usage.appendChild(usageName);
	usageList.appendChild(usage);
});
meta.appendChild(usageList);
codeList.appendChild(meta);
codes = document.createElement('codes');
Array.from(contentElmt.querySelectorAll('table tbody tr')).forEach((row) => {
	code = document.createElement('code');
	code.setAttribute('id', row.querySelector('td:nth-child(1)').innerText.trim());
	code.setAttribute('starting-date', row.querySelector('td:nth-child(4)').innerText.trim().split('-').reverse().join('-'));
	code.setAttribute('expiration-date', row.querySelector('td:nth-child(5)').innerText.trim().split('-').reverse().join('-'));
	meaning = document.createElement('meaning');
	meaning.setAttribute('lang', 'nl');
	meaningValue = document.createTextNode(row.querySelector('td:nth-child(2)').innerText.trim());
	meaning.appendChild(meaningValue);
	code.appendChild(meaning);
	explanation = document.createElement('explanation');
	explanation.setAttribute('lang', 'nl');
	explanationValue = document.createTextNode(row.querySelector('td:nth-child(3)').innerText.trim());
	explanation.appendChild(explanationValue);
	code.appendChild(explanation);
	mutation = document.createElement('mutation');
	mutation.setAttribute('mutation-date', row.querySelector('td:nth-child(6) a').dataset['content'].split('<br>')[1].trim().split('-').reverse().join('-'));
	mutationKind = document.createElement('kind');
	mutationKind.setAttribute('lang', 'nl');
	mutationKindValue = document.createTextNode(row.querySelector('td:nth-child(6) a').dataset['content'].split('<br>')[3]);
	mutationKind.appendChild(mutationKindValue);
	mutation.appendChild(mutationKind);
	code.appendChild(mutation);
	codes.appendChild(code);
});
codeList.appendChild(codes);
XMLS = new XMLSerializer();
serXML = XMLS.serializeToString(codeList);
codeList;