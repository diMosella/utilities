<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:vcl="http://ei.vektis.nl/codelists/v1_0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
		
	<xsl:output method="xml" version="1.0" omit-xml-declaration="no" 
    encoding="utf-8" media-type="application/xml" indent="yes" />
    
	<xsl:variable name="idLabel" select="'ID'" />
	<xsl:variable name="descLabel" select="'Beschrijving'" />
	<xsl:variable name="maintLabel" select="'Technisch beheer'" />
	<xsl:variable name="dateLabel" select="'Publicatiedatum'" />
    
    <xsl:variable name="listId" select="//b[text() = $idLabel and ancestor::*[@id='identificatie']]/parent::*/normalize-space(text()[last()])" />
    <xsl:variable name="baseName" select="lower-case($listId)" />
	<xsl:variable name="listDesc" select="//b[text() = $descLabel and ancestor::*[@id='identificatie']]/parent::*/normalize-space(text()[last()])" />
	<xsl:variable name="listMaint" select="//b[text() = $maintLabel and ancestor::*[@id='identificatie']]/parent::*/normalize-space(text()[last()])" />
	<xsl:variable name="listDate" select="//b[text() = $dateLabel and ancestor::*[@id='identificatie']]/parent::*/normalize-space(text()[last()])" />
    
	<xsl:template match="/">
		<xs:schema targetNamespace="http://ei.vektis.nl/codelists/v1_0" elementFormDefault="qualified" version="1.0">
			<xs:annotation>
				<xs:documentation xml:lang="nl">
					Dit is de Schema Definitie voor de Code lijst <xsl:value-of select="$listId" />. Als annotatie bij iedere toegestane code staat ook de geldigheidsperiode vermeld.
				</xs:documentation>
				<xs:documentation xml:lang="nl">
					<xsl:value-of select="$descLabel" />: <xsl:value-of select="$listDesc" />
				</xs:documentation>
				<xs:documentation xml:lang="nl">
					<xsl:value-of select="$maintLabel" />: <xsl:value-of select="$listMaint" />
				</xs:documentation>
				 <xs:documentation xml:lang="en">
					 This is the Schema Definition for Code List <xsl:value-of select="$listId" />. For each code in this list, the validity period has been included in the annotation.
				 </xs:documentation>
				<xs:appinfo source="https://www.vektis.nl">
					<vcl:publicationDate value="{$listDate}" />
				</xs:appinfo>
			</xs:annotation>
			<xs:simpleType name="{concat($baseName, 'ContentType')}">
				<xs:restriction base="xs:token">
				<xsl:for-each select="//table[ancestor::*[@id='inhoud']]/tbody/tr">
				<xs:enumeration value="{td[1]}">
					<xs:annotation>
						<xs:documentation xml:lang="nl">
							<vcl:meaning><xsl:value-of select="normalize-space(td[2])"/></vcl:meaning>
							<vcl:explanation><xsl:value-of select="normalize-space(td[3])"/></vcl:explanation>
						</xs:documentation>
						<xs:appinfo>
							<vcl:validityPeriod start="{td[4]}" end="{td[5]}" />
						</xs:appinfo>
					</xs:annotation>
				</xs:enumeration>
				</xsl:for-each>
				</xs:restriction>
			</xs:simpleType>
			<xs:complexType name="{concat($baseName, 'Type')}">
				<xs:simpleContent>
					<xs:extension base="{concat('vcl:', $baseName, 'ContentType')}">
						<xs:attribute name="metadata" type="xs:string" use="optional"/>
					</xs:extension>
				</xs:simpleContent>
			</xs:complexType>
			<xs:element name="{$baseName}" type="{concat('vcl:', $baseName, 'Type')}" />
		</xs:schema>
	</xsl:template>
</xsl:stylesheet>