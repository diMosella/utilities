<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:vcl="http://ei.vektis.nl/codelists/v1_0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
		
	<xsl:output method="xml" version="1.0" omit-xml-declaration="no" 
    encoding="utf-8" media-type="application/xml" indent="yes" />
    
	<xsl:template match="/">
		<xs:schema targetNamespace="http://ei.vektis.nl/registratieberichten/basisschema/v1_0" elementFormDefault="qualified" version="1.0">
			<xs:annotation>
				<xs:documentation xml:lang="nl">
					<vcl:description>
						Dit is de Schema Definitie voor de Code lijst <xsl:value-of select="codeList/@id" />: <xsl:value-of select="codeList/metadata/description" />
						Als annotatie bij iedere toegestane code staat ook de geldigheidsperiode vermeld.
					</vcl:description>
				</xs:documentation>
				<xs:appinfo source="{concat('https://www.vektis.nl/streams/standaardisatie/codelijsten/', codeList/@id)}">
					<vcl:schemaInfo type="CODE_LIST" id="{codeList/@id}" publicationDate="{codeList/metadata/@publication-date}" maintainer="{codeList/metadata/@maintainer}" />
				</xs:appinfo>
			</xs:annotation>

			<xs:simpleType name="{concat(translate(codeList/@id, '-', ''), 'Type')}">
				<xs:restriction base="xs:token">
				<xsl:for-each select="codeList/codes/code">
				<xs:enumeration value="{@id}">
					<xs:annotation>
						<xs:documentation xml:lang="nl">
							<vcl:meaning><xsl:value-of select="meaning/text()"/></vcl:meaning>
							<vcl:explanation><xsl:value-of select="explanation/text()"/></vcl:explanation>
							<vcl:mutation date="{mutation/@mutation-date}"><xsl:value-of select="normalize-space(mutation)"/></vcl:mutation>
						</xs:documentation>
						<xs:appinfo>
							<vcl:validityPeriod startInclusive="{@starting-date}" endExclusive="{@expiration-date}" />
						</xs:appinfo>
					</xs:annotation>
				</xs:enumeration>
				</xsl:for-each>
				</xs:restriction>
			</xs:simpleType>

		</xs:schema>
	</xsl:template>
</xsl:stylesheet>