<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://test">
	<xsl:import-schema schema-location="order.xsd"/>
	<xsl:output method="xml" version="1.0" omit-xml-declaration="no" encoding="utf-8" media-type="application/xml" indent="yes"/>
		<!-- Don't output text nodes. We just want error messages. -->
		<!-- <xsl:template match="text()"/>-->
		<!-- Make sure that element attributes get processed. -->
		<!--<xsl:template match="*">
			<xsl:apply-templates select="@*|node()"/>
		</xsl:template>-->
		<!-- Type checking templates: -->
		<!--<xsl:template match="shipAddress">
			<xsl:value-of select="./Address/country"/>
			<xsl:text>&#xa;</xsl:text>
			<xsl:if test="not(./Address instance of document-node(schema-element(tns:Address)))">
				<xsl:text>Following shipAddress value not an Address: </xsl:text>
				<xsl:value-of select="."/>
			</xsl:if>
		</xsl:template>
		<xsl:template match="shipped">
			<xsl:if test="not(string(.) castable as xs:boolean)">
				<xsl:text>
Following shipped value not a boolean: </xsl:text>
				<xsl:value-of select="."/>
			</xsl:if>
		</xsl:template>
		<xsl:template match="price">
			<xsl:if test="not(string(.) castable as xs:decimal)">
				<xsl:text>
Following price value not a decimal number: </xsl:text>
				<xsl:value-of select="."/>
			</xsl:if>
		</xsl:template>
		<xsl:template match="@shipDate">
			<xsl:if test="not(string(.) castable as xs:date)">
				<xsl:text>
Following shipDate value not a date: </xsl:text>
				<xsl:value-of select="."/>
			</xsl:if>
		</xsl:template>-->
		<xsl:template match="tns:Address">
		<test>
			<xsl:text>NOPE</xsl:text><xsl:value-of select="tns:city"/>
		</test>
	</xsl:template>
	<xsl:template match="schema-element(tns:Address)">
		<test>
			<xsl:text>YEP</xsl:text><xsl:value-of select="tns:city"/>
		</test>
	</xsl:template>
	
	<!--<xsl:template match="test:Address">
		<xsl:for-each select="Address/country">
			<xsl:text>no-schema: </xsl:text>
			<xsl:value-of select="text()"/>
			<xsl:text>&#xa;</xsl:text>
		</xsl:for-each>
	</xsl:template>-->
	<!--<xsl:template match="/PurchaseOrder">
		<doc>
			<xsl:variable name="n" select="/shipAddress/Address as "/>
			<xsl:value-of select="shipAddress/Address/country"/>
			<xsl:choose>
				<xsl:when test="$n instance of schema-element(Address)">
					<xsl:text>TRUE</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>FALSE</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</doc>
	</xsl:template>-->
	<!--<xsl:template match="document-node()">
		<xsl:message terminate="yes">Source document is not a purchase order
     </xsl:message>
	</xsl:template>-->
</xsl:stylesheet>
