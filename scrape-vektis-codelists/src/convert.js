const fs = require('fs');
const jsdom = require('jsdom');
const { XMLSerializer } = require("w3c-xmlserializer");
const immer = require('immer');
const XMLSerializerIf = XMLSerializer.interface; 
const { JSDOM } = jsdom;
const { produce } = immer;

const initialUrl = {
  url: 'https://www.vektis.nl/streams/standaardisatie/codelijsten',
  path: null
}

loadAlbum(initialUrl, null);

function formatXml(xml) {
  const PADDING = ' '.repeat(4); // indent size
  const reg = /(>)(<)(\/?)/g;
  const nsReg = /\sxmlns="[^"]+"/ig;
  const tagReg = /(<(?:\/)?\w[^>-\s]+)-([^>]+>)/ig;
  const emptyReg = /(<[^\/>]+)(>)\s*(<\/[^>]+>)/ig;
  let pad = 0;

  xml = xml.replace(nsReg, '');
  xml = xml.replace(tagReg, (match, pre, post) =>
      `${pre}${post.substr(0,1).toUpperCase()}${post.substr(1)}`);
  xml = xml.replace(reg, '$1\r\n$2$3');
  xml = xml.replace(emptyReg, '$1/$2');

  return xml.split('\r\n').map((node, index) => {
      let indent = 0;
      if (node.match(/.+<\/\w[^>]*>$/)) {
          indent = 0;
      } else if (node.match(/^<\/\w/) && pad > 0) {
          pad -= 1;
      } else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
          indent = 1;
      } else {
          indent = 0;
      }

      pad += indent;

      return PADDING.repeat(pad - indent) + node;
  }).join('\r\n');
};

function loadAlbum(url, path) {
  const album = produce(url, draftState => Object.assign(draftState, { path }));

  let folder = `results/codeLists`;

  if (!fs.existsSync(folder)) {
    console.error('CONVERT FOLDER doesn\'t yet exist!');
    return;
  }

  let fileName = `${folder}/scrape-${path || 'list'}.html`;

  if (!fs.existsSync(fileName)) {
    console.error('CONVERT FILE doesn\'t exist!');
    return;
  }

  const albumData = fs.readFileSync(fileName);

  const { document } = new JSDOM(`${albumData}`).window;

  console.log('path', path);

  if (path !== null) {
    const writefileName = `${folder}/convert-${path}.xml`;
    if (fs.existsSync(writefileName)) {
      return;
    }
    codeList = document.createElement('code-list');
    idElmt = document.getElementById('identificatie');
    usageElmt = document.getElementById('gebruikt-in-standaarden');
    contentElmt = document.getElementById('inhoud');
    codeList.setAttribute('id', Array.from(idElmt.querySelector('p:nth-of-type(2)').childNodes).pop().nodeValue.trim());
    meta = document.createElement('metadata');
    meta.setAttribute('publication-date', Array.from(idElmt.querySelector('.col-sm-6:last-child p:nth-of-type(1)').childNodes).pop().nodeValue.trim().split('-').reverse().join('-'));
    meta.setAttribute('maintainer', Array.from(idElmt.querySelector('.col-sm-6:last-child p:last-child').childNodes).pop().nodeValue.trim());
    codeName = document.createElement('name');
    nameValue = document.createTextNode(Array.from(idElmt.querySelector('p:first-child').childNodes).pop().nodeValue.trim());
    codeName.appendChild(nameValue);
    codeName.setAttribute('lang', 'nl');
    meta.appendChild(codeName);
    description = document.createElement('description');
    descriptionValue = document.createTextNode(Array.from(idElmt.querySelector('p:last-child').childNodes).pop().nodeValue.trim());
    description.appendChild(descriptionValue);
    description.setAttribute('lang', 'nl');
    meta.appendChild(description);
    usageList = document.createElement('usages');
    Array.from(usageElmt.querySelectorAll('a')).forEach((link) => {
      usage = document.createElement('usage');
      usage.setAttribute('url', link.getAttribute('href'));
      usage.setAttribute('id', link.querySelector('.specs__code').textContent.trim());
      usageName = document.createElement('name');
      nameValue = document.createTextNode(link.querySelector('.specs__label').textContent.trim());
      usageName.appendChild(nameValue);
      usageName.setAttribute('lang', 'nl');
      usage.appendChild(usageName);
      usageList.appendChild(usage);
    });
    meta.appendChild(usageList);
    codeList.appendChild(meta);
    codes = document.createElement('codes');
    Array.from(contentElmt.querySelectorAll('table tbody tr')).forEach((row) => {
      code = document.createElement('code');
      code.setAttribute('id', row.querySelector('td:nth-child(1)').textContent.trim());
      code.setAttribute('starting-date', row.querySelector('td:nth-child(4)').textContent.trim().split('-').reverse().join('-'));
      code.setAttribute('expiration-date', row.querySelector('td:nth-child(5)').textContent.trim().split('-').reverse().join('-'));
      meaning = document.createElement('meaning');
      meaning.setAttribute('lang', 'nl');
      meaningValue = document.createTextNode(row.querySelector('td:nth-child(2)').textContent.trim());
      meaning.appendChild(meaningValue);
      code.appendChild(meaning);
      explanation = document.createElement('explanation');
      explanation.setAttribute('lang', 'nl');
      explanationValue = document.createTextNode(row.querySelector('td:nth-child(3)').textContent.trim());
      explanation.appendChild(explanationValue);
      code.appendChild(explanation);
      mutation = document.createElement('mutation');
      mutation.setAttribute('mutation-date', row.querySelector('td:nth-child(6) a').dataset['content'].split('<br>')[1].trim().split('-').reverse().join('-'));
      mutationActionLabel = document.createElement('action-label');
      mutationActionLabel.setAttribute('lang', 'nl');
      mutationActionLabelValue = document.createTextNode(row.querySelector('td:nth-child(6) a').dataset['content'].split('<br>')[3] || '');
      mutationActionLabel.appendChild(mutationActionLabelValue);
      mutation.appendChild(mutationActionLabel);
      code.appendChild(mutation);
      codes.appendChild(code);
    });
    codeList.appendChild(codes);
    XMLS = new XMLSerializerIf();
    serXML = XMLS.serializeToString(codeList);

    if (!fs.existsSync(writefileName)) {
      fs.writeFileSync(writefileName, 
`<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="codeList.xsl"?>
${formatXml(serXML)}
`);
    }
  }
  if (path !== null) {
    return;
  }

  [...document.querySelectorAll("li.standards__item a.ga-standards-item span.standards__code")]
    .map(el => el.textContent)
    .filter((el, indx) => indx < 60)
    .forEach(code => loadAlbum(album, code));
}
