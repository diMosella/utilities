const https   = require('https');
const fs      = require('fs');
const jsdom   = require('jsdom');
const immer = require('immer');

const { JSDOM } = jsdom;
const { produce } = immer;

const initialUrl = {
  url: 'https://www.vektis.nl/streams/standaardisatie/codelijsten',
  path: null
}

loadAlbum(initialUrl, null);

function loadAlbum(url, path) {
  const album = produce(url, draftState => Object.assign(draftState, { path }));
  const albumUrl = `${url.url}${path ? `/${path}` : ''}`;

  let folder = `results`;

  if(!fs.existsSync(folder)) {
    console.log('SCRAPE FOLDER doesn\'t yet exist and will be created');
    fs.mkdirSync(folder);
  }

  let albumData = '';
  let fileName = `${folder}/scrape-${path || 'list'}.html`;
  if(fs.existsSync(fileName)) {
    console.error(`SCRAPE ${fileName} already exists`);
    return;
  }
  let fileAlbum = fs.createWriteStream(fileName);

  let requestAlbum = https.request(albumUrl, (response) => {
    console.log("SCRAPE RESPONSE:", response.statusCode, `${path}`, albumUrl);

    response.on('data', (data) => {
      fileAlbum.write(data);
      albumData += data;
    });

    response.on('end', () => {
      fileAlbum.close();
      if (path !== null) {
        return;
      }
      const { document } = new JSDOM(`${albumData}`).window;
      [...document.querySelectorAll("li.standards__item a.ga-standards-item span.standards__code")]
        .map(el => el.textContent)
        .forEach(code => loadAlbum(album, code));
    });
  });

  requestAlbum.on('error', (error) => {
    console.log(`SCRAPE: Problem with request: ${error.message}`);
  });

  requestAlbum.end();
}
