# Utilities 

## reorder-theme-files

This utility is used to reorder theme files. To do so, it traverses a given folder structure and a give 'incoming' folder structure.
For all files (not symbolic links) in the former folder structure matching files in the latter (incoming) will be used to overwrite them.

Start with elementary icon theme, then update /app and /places with ePapirus, and /places again with Tela

## restructure-folders

This utility renames folders with Dutch month names to ISO date format.

## scrape-awstats

This utility scrapes awstat files and collects the data from the resulting HTMLs to JSON files.

## scrape-vektis-codelist

Like scrape-awstats, this utility extracts specific HTML data from a webpage and converts it into XSDs.
